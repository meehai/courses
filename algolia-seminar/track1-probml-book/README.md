## MLCourse Seminar

Presentations will generally be created here: https://drive.google.com/drive/folders/1Xx2TJXNIpvKgr8QUglQQCEa9BqYqKJjF?usp=sharing

Assignments will generally end up here as standalone scripts or notebooks.

## Seminars

_Week 1 - 20210304_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week1%20-%2020210304/week1%20-%2020210304.pptx
* Video: https://www.youtube.com/watch?v=L5ezPCl3XIg

_Week 2 - 20210311_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week2%20-%2020210311/week2%20-%2020210311.pptx
* Video: https://www.youtube.com/watch?v=grY15VnJC-c

_Week 3 - 20210318_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week3%20-%2020210318/week3%20-%2020210318.pptx
* Video: https://www.youtube.com/watch?v=BG9QHW29FMA

_Week 4 - 20210401_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week4%20-%2020210401/week4%20-%2020210401.pptx
* Video: https://www.youtube.com/watch?v=rrC5CiPfqnE

_Week 5 - 20210415_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week5%20-%2020210408/week5%20-%2020210415.pptx
* Video: https://www.youtube.com/watch?v=YVd477jUJ58

_Week 6 - 20210513_.
* Presentation: https://gitlab.com/mihaicristianpirvu/courses/-/blob/master/algolia-seminar/track1-probml-book/Week6%20-%2020210513/week6%20-%2020210513.pptx
* Video: https://www.youtube.com/watch?v=kN_zw1oKWYY
