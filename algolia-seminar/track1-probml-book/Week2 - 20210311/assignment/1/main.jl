using Base
using ArgParse
using Debugger

include("concepts.jl")
include("do_plots.jl")

function likelihood(concept, dataset)
    N = size(concept)
    allIn = [x in concept for x in dataset]
    A = prod(allIn)
    B = 1.0 / length(concept)
    C = length(dataset)
    return A * B ^ C
end

# Normalize a given distribution!
function normalize(X)
    K, V = keys(X), values(X)
    normedV = V ./ sum(V)
    normedX = Dict(zip(K, normedV))
    @assert isapprox(sum(values(normedX)), 1.0)
    return normedX
end

function getArgs()
    s = ArgParseSettings()
    
    @add_arg_table s begin
        "--dataset"
            required = true
        "--plot"
            required = true
    end
    args = parse_args(s)
    @assert args["dataset"] in ("1", "2")
    return args
end

function main()
    args = getArgs()

    println("--Data--")
    D = args["dataset"] == "1" ? [16] : [16, 8, 2, 64]
    println("Dataset: ", D)

    println("--Concepts--")
    concepts = getConcepts()
    Keys = keys(concepts)
    nElementsConcepts = Dict(zip(Keys, map(x -> length(x), values(concepts))))
    println(nElementsConcepts)

    println("--Priors--")
    priors = normalize(getPriors())
    println(priors)

    println("--Likelihood--")
    likelihoods = [likelihood(concepts[k], D) for k in Keys]
    likelihoods = Dict(zip(Keys, likelihoods))
    lEven = likelihoods["even"]
    lPowers = likelihoods["Powers of 2"]
    lRatio = lPowers / lEven
    println("Likelihood of even with dataset ", D, " : ", lEven)
    println("Likelihood of powers of two with dataset ", D, " : ", lPowers)
    println("Likelihood ratio (powers/even): ", lRatio)

    println("--Posterior--")
    posteriors = [priors[k] * likelihoods[k] for k in Keys]
    posteriors = Dict(zip(Keys, posteriors))
    posteriors = normalize(posteriors)
    println(posteriors)

    if args["plot"] == "1"
        K, priors = keys(priors), values(priors)
        likelihoods = [likelihoods[k] for k in K]
        posteriors = [posteriors[k] for k in K]
        doPlots(K, priors, likelihoods, posteriors)
        show()
    end
end

main()