using PyPlot
include("head_tails.jl")
include("bayes.jl")

function doPlots(concepts, priors, likelihoods)
    @assert length(priors) == length(likelihoods) == length(posteriors)
    X = 1:length(priors)

    fig, ax = subplots(1, 2)
    ax[1].barh(X, priors)
    ax[2].barh(X, likelihoods)
    ax[3].barh(X, posteriors)
    ax[1].set_yticklabels(concepts)
    ax[2].set_yticklabels(repeat([""], length(concepts)))
    ax[3].set_yticklabels(repeat([""], length(concepts)))
    ax[1].set_yticks(1:length(priors))
    ax[2].set_yticks(1:length(priors))
    ax[3].set_yticks(1:length(priors))
    return fig
end

function experiment(N, P)
    e = zeros(N)
    ix = trunc(Int, P * N)
    e[1:ix] .= 1
    return e
end

function main()
    concepts = getConcepts(10)
    X = 1:length(concepts)
    ax = subplots(1, 3)[2]
    for i in 1:3
        E = experiment(10, 0.25 * (i - 1))
        likelihood = fLikelihood(E, concepts)
        ax[i].barh(X, likelihood)
        ax[i].set_yticks(1:length(concepts))
        ax[i].set_yticklabels(concepts)
        Str = string("Data: {heads: ", headTails(E)["heads"], ", tails:", headTails(E)["tails"], "}")
        ax[i].set_title(Str)
    end
    show()
end

main()