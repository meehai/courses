include("bayes.jl")
include("../1/do_plots.jl")
include("head_tails.jl")

function main()
    nConcepts = 10
    nData = 5
    lr = 0.1

    concepts = getConcepts(nConcepts)
    println("Concepts:", concepts)
    prior = fPrior(concepts)
    println("Prior:", prior)

    e = []
    # e = vcat(e, doOneExperiment(nData))
    for i in 1:100
        e = vcat(e, doOneExperiment(nData))
        likelihood, posterior = bayes(concepts, prior, e)
        posterior = normalize(prior .+ (posterior .- prior) .* lr)
        ix = argmax(posterior)

        println("--- Iteration $i. N Data: ", length(e), " ---")
        println("Likelihood: ", likelihood)
        println("Posterior: ", posterior)
        println(" Most likely concept: ", concepts[ix], " with posterior: ", posterior[ix])

        fig = doPlots(concepts, prior, likelihood, posterior)
        suptitle(string("Iteration $i. Data: ", headTails(e)))
        # show()
        savefig("$i.png")
        prior = posterior
    end

end

main()