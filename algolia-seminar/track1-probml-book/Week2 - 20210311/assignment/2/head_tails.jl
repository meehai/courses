using Random
using Base

function normalize(X)
    return X ./ sum(X)
end

# prior -> a distribution probability over "fairness". P(H) == P("fairness").
# If headProb == 0.9 and N = 10, means we get a probablity distribution of [0, 0, .., 1, 0] (10 entries, 9th = 100%)
function getConcepts(N)
    return Array(0 : 1 : N) ./ N
end

# P(H)
function fPrior(concepts)
    prior = ones(length(concepts))
    ix = Int(round(0.9 * length(concepts)))
    prior[ix] = 100
    prior = normalize(prior)
    return prior
end

# The likelihood function P(D|H) give a fairness value P(H). We model this as a binomial distribution
function fLikelihood(experiment::Array, concept::Float64)
    @assert concept >= 0 and concept <= 1
    # heads = length(filter(x -> x == 1, experiment))
    # tails = length(filter(x -> x == 0, experiment))
    mean(x) = sum(x) / length(x)
    A = mean(experiment)
    B = abs(concept - A)
    C = 1 - B
    D = C ^ length(experiment)
    return D
end


# P(D) for some N
function doOneExperiment(N=100, P=0.5)
    return rand(N) .> P
end

function headTails(e)
    heads = length(filter(x -> x == 1, e))
    tails = length(filter(x -> x == 0, e))
    return Dict("heads" => heads, "tails" => tails)
end
