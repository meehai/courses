function fLikelihood(experiment::BitArray, prior::Array)
    return [fLikelihood(Array(experiment), _prior) for _prior in prior]
end

function fLikelihood(experiment::Array, prior::Array)
    return [fLikelihood(experiment, _prior) for _prior in prior]
end

function fPosterior(prior::Array, likelihood::Array)
    posterior = prior .* likelihood
    posterior = normalize(posterior)
    return posterior
end

function bayes(concepts::Array, prior::Array, experiment::Array)
    likelihood = fLikelihood(experiment, concepts)
    posterior = fPosterior(prior, likelihood)
    return likelihood, posterior
end
