using PyPlot
using Seaborn

function doPlots(concepts::Any, priors::Array{Float64}, likelihoods::Array{Float64}, posteriors::Array{Float64})
	return doPlots(Array([x for x in concepts]), priors, likelihoods, posteriors)
end

function doPlots(concepts::Array, priors::Array{Float64}, likelihoods::Array{Float64}, posteriors::Array{Float64})
	@assert length(priors) == length(likelihoods) == length(posteriors)
	X = 1:length(priors)
	Range = range(0, stop=1, length=5)

	fig, ax = subplots(1, 3)
	ax[1].barh(X, priors)
	ax[1].set_yticklabels(concepts)
	ax[1].set_yticks(1:length(priors))
	ax[1].set_xticks(Range)
	ax[1].set_xticklabels(Range)
	ax[1].set_title("Prior")

	ax[2].barh(X, likelihoods)
	ax[2].set_yticklabels(repeat([""], length(concepts)))
	ax[2].set_yticks(1:length(priors))
	# ax[2].set_xticks(Range)
	# ax[2].set_xticklabels(Range)
	ax[2].set_title("Likelihood")

	ax[3].barh(X, posteriors)
	ax[3].set_yticklabels(repeat([""], length(concepts)))
	ax[3].set_yticks(1:length(priors))
	ax[3].set_xticks(Range)
	ax[3].set_xticklabels(Range)
	ax[3].set_title("Posterior")
	return fig
end

function doPlots2D(concepts2D, prior2D, likelihood2D, posterior2D)
	@assert size(prior2D) == size(likelihood2D) == size(posterior2D)
	H, PF = concepts2D
	nH, nPF = size(prior2D)
	# Reverse them because sns.heatmap loves to put my matrix reverse row-wise and it doesn't match the barh plot. 
	H = reverse(H)
	prior2D, likelihood2D, posterior2D = map(x -> reverse(x, dims=1), [prior2D, likelihood2D, posterior2D])

	fig, ax = subplots(1, 3)
	heatmap(prior2D, ax=ax[1], xticklabels=PF, yticklabels=H)
	heatmap(likelihood2D, ax=ax[2], xticklabels=PF)
	heatmap(posterior2D, ax=ax[3], xticklabels=PF)
	ax[1].set_title("Prior")
	ax[2].set_title("Likelihood")
	ax[2].yaxis.set_visible(false)
	ax[3].set_title("Posterior")
	ax[3].yaxis.set_visible(false)
	return fig
end