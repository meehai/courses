include("do_plots.jl")
include("hypotheses.jl")
include("prime_freakness.jl")
using .Hypotheses
using .PrimeFreakness
using LinearAlgebra
using Printf

function posterior(prior::Array, likelihood::Array)
    normalize(x) = x ./ sum(x)
    posterior = prior .* likelihood
    posterior = normalize(posterior)
    return posterior
end

function getHypotheses(e)
    hConcepts = Hypotheses.getConcepts()
    hPriors = [Hypotheses.prior(concept) for concept in hConcepts]
    hLikelihoods = [Hypotheses.likelihood(e, concept) for concept in hConcepts]
    hPosteriors = posterior(hPriors, hLikelihoods)
    # println(hConcepts)
    # println(hPriors)
    # println(hLikelihoods)
    # println(hPosteriors)
    return hConcepts, hPriors, hLikelihoods, hPosteriors
end

function getPrimeFreakness(e)
    pfConcepts = PrimeFreakness.getConcepts()
    pfPriors = [PrimeFreakness.prior(concept) for concept in pfConcepts]
    pfLikelihoods = [PrimeFreakness.likelihood(e, c) for c in pfConcepts]
    pfPosteriors = posterior(pfPriors, pfLikelihoods)
    # println(pfConcepts)
    # println(pfPriors)
    # println(pfLikelihoods)
    # println(pfPosteriors)
    return pfConcepts, pfPriors, pfLikelihoods, pfPosteriors
end

function main()
    e = [7, 11, 33]

    hConcepts, hPriors, hLikelihoods, hPosteriors = getHypotheses(e)
    pfConcepts, pfPriors, pfLikelihoods, pfPosteriors = getPrimeFreakness(e)

    # 1D histograms
    doPlots(hConcepts, hPriors, hLikelihoods, hPosteriors)
    suptitle("Dataset: $e")
    doPlots(pfConcepts, pfPriors, pfLikelihoods, pfPosteriors)
    suptitle("Dataset: $e")

    # # 2D confusion matrix
    prior2D = hPriors * pfPriors'
    likelihood2D = hLikelihoods * pfLikelihoods'
    posterior2D = posterior(prior2D, likelihood2D)
    doPlots2D([hConcepts, pfConcepts], prior2D, likelihood2D, posterior2D)
    suptitle("Dataset: $e")
    show()
end

main()