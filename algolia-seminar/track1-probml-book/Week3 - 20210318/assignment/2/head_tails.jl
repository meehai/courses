module HeadTails

# P(D) for some N
function doOneExperiment(N=100, P=0.5)
    return Array{Float64}(rand(N) .> P)
end

# P(H) = gauss(H, u=0.9, std=0.1)
function prior()
    std = 0.1
    u = 0.7
    # ct = 1 / (std * sqrt(2 * pi))
    ct = 1 / (std * sqrt(2 * pi))
    gauss(x) = ct * exp( (-1 / 2) * ((x - u) / std) ^ 2 )

    C = sum([gauss(x) * 0.01 for x in range(-10, stop=10, step=0.01)])
    @assert isapprox(C, 1.00)

    return gauss
end

# P(Y|H=h)
function likelihood(Y::Array, h::Float64)
    if h < 0 || h > 1
        return 0
    end

    mean(x) = sum(x) / length(x)
    A = mean(Y)
    B = abs(h - A)
    C = 1 - B
    D = 10
    # D = min(10, log(length(Y)))
    E = C ^ D 
    return E
end

end