using PyPlot
using Seaborn

discretize(f, start::Number, stop::Number; step::Number) = [f(x) for x in range(start, stop, step=step)]

function discretize_2d(f, start::Number, stop::Number; step::Number)
    Range = range(start, stop, step=step)
    d1 = Array([y -> f(x, y) for x in Range])
    d2 = Array([_d1(x) for _d1 in d1 for x in Range])
    d2 = reshape(d2, length(d1), length(d1))
    return d2
end

function doPlots(g1, g2, Range)
    labels = ["" for _ in Range]
    for i in range(1, length=length(Range))
        if i % 5 == 0
            labels[i] = "$(Range[i])"
        end
    end

    g3(x::Number, y::Number) = g1(x) * g2(y)
    d1 = discretize(g1, -5, 5, step=0.1)
    d2 = discretize(g2, -5, 5, step=0.1)
    print(g3(1, 2))
    d3 = discretize_2d(g3, -5, 5, step=0.1)

    fig = figure()
    ax = fig.add_subplot(2, 3, 1)
    ax.barh(Range, d1, height=0.05)
    ax = fig.add_subplot(2, 3, 2)
    ax.barh(Range, d2, height=0.05)
    ax = fig.add_subplot(2, 3, 3)
    heatmap(d3, ax=ax, xticklabels=labels, yticklabels=labels)

    ax = fig.add_subplot(2, 3, 4)
    ax.plot(d1, Range)
    ax = fig.add_subplot(2, 3, 5)
    ax.plot(d2, Range)
    ax = fig.add_subplot(2, 3, 6, projection="3d")
    RangeX = reshape(repeat(Range, length(Range)), length(Range), length(Range))
    RangeY = reshape(repeat(Range', length(Range)), length(Range), length(Range))
    println(size(RangeX))
    println(size(RangeY))
    println(size(d3))
    ax.plot_wireframe(RangeX, RangeY, d3)
end

function doPlotsBayes(g1, g2, Range)
    labels = ["" for _ in Range]
    for i in range(1, length=length(Range))
        if i % 5 == 0
            labels[i] = "$(Range[i])"
        end
    end

    g3(x::Number, y::Number) = g1(x) * g2(y)
    d1 = discretize(g1, -5, 5, step=0.1)
    d2 = discretize(g2, -5, 5, step=0.1)
    # print(g3(1, 2))
    d3 = discretize_2d(g3, -5, 5, step=0.1)

    fig = figure()
    ax = fig.add_subplot(1, 4, 1)
    ax.plot(d1, Range)
    ax = fig.add_subplot(1, 4, 2)
    ax.plot(d2, Range)
    ax = fig.add_subplot(1, 4, 3)
    heatmap(d3, ax=ax, xticklabels=labels, yticklabels=labels)
    ax = fig.add_subplot(1, 4, 4, projection="3d")
    RangeX = reshape(repeat(Range, length(Range)), length(Range), length(Range))
    RangeY = reshape(repeat(Range', length(Range)), length(Range), length(Range))
    println(size(RangeX))
    println(size(RangeY))
    println(size(d3))
    ax.plot_wireframe(RangeX, RangeY, d3)
end
