include("plots.jl")

gauss(x; std, u) = 1 / (std * sqrt(2 * pi)) * exp( (-1 / 2) * ((x - u) / std) ^ 2 )

Range = range(-5, 5, step=0.1)
g1 = x -> gauss(x, std=1, u=-1.8)
g2 = x -> gauss(x, std=2, u=1.3)
doPlots(g1, g2, Range)
show()